## This is an extended version of React Tic Tac Toe Tutorial

[Tutorial link](https://reactjs.org/tutorial/tutorial.html)


Advanced features:

- [x] Display the location for each move in the format (col, row) in the move history list.

- [x] Bold the currently selected item in the move list.

- [x] Rewrite Board to use two loops to make the squares instead of hardcoding them.

- [ ] Add a toggle button that lets you sort the moves in either ascending or descending order.

- [ ] When someone wins, highlight the three squares that caused the win.

- [ ] When no one wins, display a message about the result being a draw.


To view this project first install dependencies with `npm install` and then run:

### `npm start`

Open [http://localhost:3000](http://localhost:3000) to view it in the browser.